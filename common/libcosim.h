#ifndef LIBCOSIM_H
#define LIBCOSIM_H

/* Build parameters:

   #define COSIM_SILENT		- disable all console messages
   #define COSIM_HOST		- build to run on Unix host
   #define COSIM_TARGET		- build to run on ARM target model

*/

/* successfully exit co-simulation */
void cosim_success(void);

/* exit co-simulation with failure exit code CODE */
void cosim_fail(int code);

#ifndef COSIM_SILENT
/* print a message with format FMT to the co-simulation console */
void cosim_printf(char *fmt, ...);
#else /* COSIM_SILENT */
/* run silent */
#define cosim_printf(FMT, ARGS...)	do { ; } while (0)
#endif /* COSIM_SILENT */

#ifndef NULL
#define NULL	((void *)0)
#endif /* NULL */

#endif /* LIBCOSIM_H */
